fio\_parser
==========

Simple library for parsing monthly account statements from the Fio Bank CZ. Supports CSV and GPC formats. Parses transaction emails too.

Installation
============

	# gem install fio_parser

Usage
=====

Be careful GPC and CSV doesn't return same values. For example: CSV doesn't contain specific and constant symbol. Message from the GPC is shortened.

```ruby
  require 'rubygems'
  require 'fio_parser'

  # CSV
  fio_csv = FioParser.parse_csv 'path_to_csv_file.csv'
  fio_csv.transactions.each do |t|
    puts "Date: #{t.date}"
    puts "Total: #{t.total}"
    puts "Contra account: #{t.contra_acc} / #{t.contra_acc_bc}"
    puts "Contra account name: #{t.contra_acc_name}"
    puts "Variable symbol: #{t.vs}"
    puts "Transaction type: #{t.type}" # :transfer or :card
    puts "Message: #{t.message}"
    puts "Comment: #{t.comment}"
  end
 
  # GPC
  fio_gpc = FioParser.parse_gpc 'path_to_gpc_file.gpc'
  fio_gpc.transactions.each do |t|
    puts "Date: #{t.date}"
    puts "Total: #{t.total}"
    puts "Contra account: #{t.contra_acc} / #{t.contra_acc_bc}"
    puts "Variable symbol: #{t.vs}"
    puts "Constant symbol: #{t.cs}"
    puts "Specific symbol: #{t.ss}"
    puts "Type: #{t.type}" # 1, 2, 4, 5 see http://www.fio.cz/docs/cz/struktura-gpc.pdf
    puts "Message: #{t.message}"
  end

  # Email
  fio_email = FioParser.parse_email 'string_with_email_body_or_path_to_file_with_email_body'
  puts "Total: #{fio_email.total}"
  puts "Account: #{fio_email.account}"
  puts "Contra account: #{fio_email.contra_acc} / #{fio_email.contra_acc_bc}"
  puts "Variable symbol: #{fio_email.vs}"
  puts "Constant symbol: #{fio_email.cs}"
  puts "Specific symbol: #{fio_email.ss}"
  puts "Transaction type: #{fio_email.type}" # :transfer or :card
  puts "Balance: #{fio_email.balance}"
  puts "Message: #{fio_email.message}"
```
License
=======

Copyright (c) 2012 cervajz@cervajz.com

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
