# encoding: UTF-8

# fio_parser is simple library for parsing monthly account statements from the Fio Bank CZ.
# Supports CSV and GPC formats.
# Support for the transaction email.
#
# Author: Jaromír "Cervajz" Čevenka
# http://www.cervajz.com
#
# Copyright © 2012 cervajz@cervajz.com
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the “Software”), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

require 'ostruct'
require 'date'

module GPCRowParser
  def delete_zeros str
    str.gsub /\A0*/, ''
  end

  def gpc_bank_code row
    result = row[71..80][2..5]
    result.each_char { |ch| return result if (ch != '0') }
    ''
  end

  def gpc_constant_symbol row
    result = delete_zeros row[71..80][6..10]
    result.each_char { |ch| return result if (ch != '0') }
    ''
  end

  def gpc_specific_symbol row
    result = row[81..90]
    result.each_char { |ch| return result if (ch != '0') }
    ''
  end

  def gpc_total row
    result = row[48..59].to_i / 100.0
    result = 0 - result if row[60].to_i == 1
    result
  end

  def gpc_date row
    Date.strptime row[122..127], '%d%m%y'
  end
end

class FioParser
  include GPCRowParser
  attr_reader :transactions

  class << self
    def parse_csv csv_file_path
      new csv_file_path, :csv
    end

    def parse_gpc gpc_file_path
      new gpc_file_path, :gpc
    end

    def parse_email email_body
      new email_body, :email
    end
  end

  def initialize file_path, type
    @transactions = []

    case type
      when :csv
        parse_csv file_path
      when :gpc
        parse_gpc file_path
      when :email
        parse_email file_path
      else
        Exception.raise 'Unknown input type'
    end
  end

  def incomes
    result = []
    @transactions.each { |t| result.push(t) if t.total > 0.0 }
    result
  end

  def total_income
    result = 0
    incomes.each { |i| result += i.total }
    result.round(2)
  end

  def total_expenses
    result = 0
    expenses.each { |i| result += i.total }
    result.abs.round(2)
  end

  def expenses
    result = []
    @transactions.each { |t| result.push(t) if t.total < 0.0 }
    result
  end

  def first
    @transactions.first
  end

  def last
    @transactions.last
  end

  private

  def parse_csv csv_file
    content = false

    file_rows(csv_file).each do |row|
      if content && contain_transaction?(row)
        @transactions.push parse_csv_row row
      end

      content = row.downcase.include? 'převod;' unless content
    end
  end

  def parse_gpc gpc_file

    file_rows(gpc_file).each do |row|
      if row[0..2].downcase.include?('075')
        @transactions.push parse_gpc_row(row)
      end
    end
  end

  def parse_email email
    if File.exist?(email)
      email_body = file_rows email
    else
      email_body = string_rows email
    end

    class << self
      attr_reader :account
      attr_reader :total
      attr_reader :vs
      attr_reader :message
      attr_reader :balance
      attr_reader :type
      attr_reader :contra_acc
      attr_reader :contra_acc_bc
      attr_reader :ss
      attr_reader :cs
    end

    income = true

    email_body.each_with_index do |line, index|
      out = get_string_from_colon line

      case index
        when 0 # Account nr.
          income = mail_income? line
          @account = out
        when 1 # Total
          @total = mail_total out, income
        when 2 # Variable symbol
          @vs = out
        when 3 # Messsage
          @message = out
        when 4 # Balance
          @balance = get_total out
        when 5 # Contra account & payment type
          @type = mail_transaction_type out
          if @type == :transfer
            @contra_acc = out.split('/')[0]
            @contra_acc_bc = out.split('/')[1]
          else
            @contra_acc = ''
            @contra_acc_bc = ''
          end
        when 6 # Specific symbol
          @ss = out
        when 7 # Constant symbol
          @cs = out
        else
          #
      end
    end
  end

  def parse_csv_row row
    columns = row.split ';'

    t = OpenStruct.new
    t.date = Date.strptime columns[1], '%d.%m.%Y'
    t.total = get_total columns[2]
    t.contra_acc = columns[3]
    t.contra_acc_bc = columns[4]
    t.vs = columns[5]
    t.type = get_transaction_type columns[6]
    t.contra_acc_name = columns[7]
    t.message = columns[8]
    t.comment = columns[9]

    t
  end

  def parse_gpc_row row
    t = OpenStruct.new

    t.date = gpc_date row
    t.total = gpc_total row
    t.contra_acc = delete_zeros row[19..34]
    t.contra_acc_bc = gpc_bank_code row
    t.vs = delete_zeros row[61..70]
    t.cs = gpc_constant_symbol row
    t.ss = gpc_specific_symbol row
    t.type = row[60].to_i
    t.message = row[97..116]

    t
  end

  def contain_transaction? row
    !(row.strip.empty? || row.downcase.include?('suma;'))
  end

  def get_transaction_type str
    if ['převodem', 'prevodem', 'bezhotovostní', 'bezhotovostni', 'připsaný úrok'].any? { |w| str.downcase.include?(w) }
      return :transfer
    end

    if %w(kartou).any? { |w| str.include?(w) }
      return :card
    end

    :other
  end

  def file_rows file
    result = []
    File.open(file).each_line { |line| result.push convert_line line }
    result
  end

  def string_rows str
    result = []
    str.each_line { |line| result.push convert_line line }
    result
  end

  def convert_line str
    if str.encoding.to_s.downcase == "utf-8"
      str
    else
      e_c = Encoding::Converter.new 'cp1250', 'utf-8'
      e_c.convert str
    end
  end

  def get_string_from_colon str
    str.sub(/^.*?:/i, '').strip
  end

  def get_total str
    str.delete(' ').gsub(',', '.').to_f
  end

  def mail_total str, income
    total = get_total str
    total = 0 - total unless income
    total
  end

  def mail_income? str
    ['příjem', 'prijem'].any? { |w| str.downcase.include?(w) }
  end

  def mail_transaction_type str
    str.downcase.include?('kartou') ? :card : :transfer
  end
end