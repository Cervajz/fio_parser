Gem::Specification.new do |s|
  s.name        = 'fio_parser'
  s.version     = '0.2.0'
  s.date        = '2012-10-24'
  s.summary     = "Gem for parsing monthly statements from the Fio Bank CZ. Parses transaction emails too."
  s.description = "Gem for parsing monthly statements from the Fio Bank CZ. Supports CSV, GPC and email format."
  s.authors     = ["Jaromír Červenka"]
  s.email       = 'cervajz@cervajz.com'
  s.files       = ["lib/fio_parser.rb"]
  s.homepage    = 'https://github.com/Cervajz/fio_parser'
end
